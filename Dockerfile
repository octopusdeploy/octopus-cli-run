FROM octopusdeploy/octo:9.1.7

RUN apk update && apk upgrade && apk add --no-cache bash git curl jq

COPY pipe /
RUN chmod a+x /*.sh

ENTRYPOINT ["/pipe.sh"]
