#!/usr/bin/env bash

# Runs commands from the Octopus CLI (built on top of the Octopus Deploy REST API)
#
# Required globals:
#   CLI_COMMAND
#
# Optional globals:
#   EXTRA_ARGS (default: empty array)
#   DEBUG (default: "false")

SCRIPT_PATH=$(dirname "$0")
source "${SCRIPT_PATH}"/common.sh
source "${SCRIPT_PATH}"/pack.sh
source "${SCRIPT_PATH}"/push.sh
source "${SCRIPT_PATH}"/build-information.sh
source "${SCRIPT_PATH}"/create-release.sh
source "${SCRIPT_PATH}"/deploy-release.sh
source "${SCRIPT_PATH}"/run-runbook.sh

# See: https://octopus.com/docs/octopus-rest-api/octopus-cli#octoCommandLine-Commands
SUPPORTED_COMMANDS=("pack" "push" "build-information" "create-release" "deploy-release" "run-runbook")

# Default parameters
DEBUG=${DEBUG:="false"}

enable_debug

# required parameters
if [[ -z ${CLI_COMMAND} ]]; then
  fail "No Octopus CLI command supplied. Please pass via the CLI_COMMAND environment variable."
fi

if [[ ! " ${SUPPORTED_COMMANDS[@]} " =~ " ${CLI_COMMAND} " ]]; then
  printf -v COMMAND_LIST "%s, " "${SUPPORTED_COMMANDS[@]}"
  fail "Command: $CLI_COMMAND is not supported. Please choose one of: ${COMMAND_LIST%??}"
fi

debug "Creating EXTRA_ARGS array"
init_array_var 'EXTRA_ARGS'

# Execute command
run ${CLI_COMMAND}

# Check status
if [[ "${status}" -eq 0 ]]; then
  success "Command: $CLI_COMMAND completed successfully."
else
  fail "✖ Command: $CLI_COMMAND failed. Please see the output for further details."
fi