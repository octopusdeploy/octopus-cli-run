#!/usr/bin/env bash

# Main entry point
push() {

    debug "Creating PACKAGES Array"
    init_array_var 'PACKAGES'

    PACKAGES_LENGTH=${#PACKAGES[@]}
    if [[ "${PACKAGES_LENGTH}" -eq 0 ]]; then
        fail "ERROR: PACKAGES variable missing any values."
    fi

    # Check for mandatory values.
    if isNotSet "${OCTOPUS_SERVER}"; then
        fail "ERROR: OCTOPUS_SERVER variable missing."
    fi
    if isNotSet "${OCTOPUS_APIKEY}"; then
        fail "ERROR: OCTOPUS_APIKEY variable missing."
    fi
    
    args=()
    for package in "${PACKAGES[@]}"
    do
        args+=("--package=${package}")
    done

    args+=("--server=${OCTOPUS_SERVER}")
    args+=("--apiKey=${OCTOPUS_APIKEY}")

    optionalArgs=()

    if isSet "${OCTOPUS_SPACE}"; then
        optionalArgs+=("--space=${OCTOPUS_SPACE}")
    fi

    if isSet "${OVERWRITE_MODE}"; then
        if [[ ! " ${overwrite_modes[@]} " =~ " ${OVERWRITE_MODE} " ]]; then
            fail "ERROR: OverwriteMode '$OVERWRITE_MODE' is not supported. Please choose one of: ${overwrite_list%??}"
        else
            optionalArgs+=("--overwrite-mode=${OVERWRITE_MODE}")
        fi
    fi

    info "Running: octo push ${args[@]} ${optionalArgs[@]} ${EXTRA_ARGS[@]}"
    octo push "${args[@]}" "${optionalArgs[@]}" "${EXTRA_ARGS[@]}"
}