#!/bin/bash

# Begin Standard 'imports'
set -e
set -o pipefail

overwrite_modes=("FailIfExists" "OverwriteExisting" "IgnoreIfExists")
printf -v overwrite_list "%s, " "${overwrite_modes[@]}"

gray="\\e[37m"
blue="\\e[36m"
yellow="\\e[33m"
red="\\e[31m"
green="\\e[32m"
reset="\\e[0m"

#######################################
# echoes a message in blue
# Globals:
#   None
# Arguments:
#   Message
# Returns:
#   None
#######################################
info() { echo -e "${blue}INFO: $*${reset}"; }

#######################################
# echoes a message in yellow
# Globals:
#   None
# Arguments:
#   Message
# Returns:
#   None
#######################################
warn() { echo -e "${yellow}WARN: $*${reset}"; }

#######################################
# echoes a message in red
# Globals:
#   None
# Arguments:
#   Message
# Returns:
#   None
#######################################
error() { echo -e "${red}ERROR: $*${reset}"; }

#######################################
# echoes a message in grey. Only if debug mode is enabled
# Globals:
#   DEBUG
# Arguments:
#   Message
# Returns:
#   None
#######################################
debug() {
  if [[ "${DEBUG}" == "true" ]]; then
    echo -e "${gray}DEBUG: $*${reset}";
  fi
}

#######################################
# echoes a message in green
# Globals:
#   None
# Arguments:
#   Message
# Returns:
#   None
#######################################
success() { echo -e "${green}✔ $*${reset}"; }

#######################################
# echoes a message in red and terminates the programm
# Globals:
#   None
# Arguments:
#   Message
# Returns:
#   None
#######################################
fail() { echo -e "${red}$*${reset}"; exit 1; }

## Enable debug mode.
enable_debug() {
  if [[ "${DEBUG}" == "true" ]]; then
    info "Enabling debug mode."
    set -x
  fi
}

#######################################
# Execute a command, saving its output (to a file) and exit status code.
# Globals:
#   status: Exit status of the command that was executed.
#   output_file: Local path with captured output generated from the command.
# Arguments:
#   command: command to run
# Returns:
#   None
#######################################
run() {
  output_file="/var/tmp/pipe-$(date +%s)-$RANDOM.log"
  set +e
  "$@" | tee "$output_file"
  status=$?
  set -e
}

#######################################
# Initialises a single array variable from variables matching supplied arg name
# Globals:
#   None
# Arguments:
#   Variables to include in array
# Returns:
#   Array variable
#######################################
init_array_var() {
    local array_var=${1}
    local count_var=${array_var}_COUNT
    for (( i = 0; i < ${!count_var:=0}; i++ ))
    do
      eval ${array_var}[$i]='$'${array_var}_${i}
    done
}

isSet() {
    [ ! -z "${1}" ]
}

isNotSet() {
    [ -z "${1}" ]
}

isFlagSet() {
    [ ! -z "${1}" ] && [ "${1}" == "true" ]
}

# End standard 'imports'