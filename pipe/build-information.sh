#!/usr/bin/env bash

getSingleGitCommit() {
    info "Adding /opt/atlassian/pipelines/agent/build as safe.directory to git config"
    git config --global --add safe.directory /opt/atlassian/pipelines/agent/build

    info "Getting commit info for $commitHash"
	git log --pretty=oneline $commitHash -1 > git-commits.log
}

getMultipleGitCommits() {
    info "Adding /opt/atlassian/pipelines/agent/build as safe.directory to git config"
    git config --global --add safe.directory /opt/atlassian/pipelines/agent/build
    
    info "Getting commit info between $prevHash and $commitHash"
    git log --pretty=oneline "$prevHash".."$commitHash" > git-commits.log
}

getCommitInfo() {

    if [[ $buildNumber -eq 1 ]] || [[ $MULTIPLE_COMMITS == "false" ]]; then
        getSingleGitCommit
        if [[ $? -ne 0 ]]; then
            error "Could not retrieve commit info for $commitHash. Either buildNumber=1, or MULTIPLE_COMMITS=FALSE"
            return 1
        fi
    else

        pipelineBaseUrl="https://api.bitbucket.org/2.0/repositories/$workspace/$repository/pipelines"
        optionalAuthArgs=()

        if [[ ! -z "$AUTH_USERNAME" ]] && [[ ! -z "$AUTH_PASSWORD" ]]; then
            debug "Credentials provided for retrieving build number and previous commit hash"
            optionalAuthArgs=("-u")
            optionalAuthArgs+=("${AUTH_USERNAME}:${AUTH_PASSWORD}")
        else
            debug "No credentials provided, or one of AUTH_USERNAME or AUTH_PASSWORD are null/empty"
        fi

        # First test to see if we need credentials e.g. 404 returned
        info "Getting previousBuildNumber from: $pipelineBaseUrl/?sort=-created_on"
        pipelineBuildStatusCode=$(curl "${optionalAuthArgs[@]}" -s -X GET "$pipelineBaseUrl/?sort=-created_on" -o pipelinebuilds.json -w '%{http_code}\n')
        debug "https://api.bitbucket.org/2.0/repositories/$workspace/$repository/pipelines returned HTTP status: $pipelineBuildStatusCode"
        
        if [[ $pipelineBuildStatusCode -ne 200 ]]; then
            error "The pipeline build URL (https://api.bitbucket.org/2.0/repositories/$workspace/$repository/pipelines) returned $pipelineBuildStatusCode. Is this a private repository? If so, set AUTH_USERNAME and AUTH_PASSWORD and try again."
            return 1
        fi
        
        pipelineBuildJson=$(<pipelinebuilds.json)
        
        # Delete temporary pipeline file
        if [[ -f "pipelinebuilds.json" ]]; then
            debug "Deleting temporary pipelinebuilds.json file"
            rm pipelinebuilds.json
        fi      

        previousbuildNumber=$(echo "${pipelineBuildJson}" | jq \
            --arg branchName "$gitBranch" \
            --arg buildNumber $buildNumber \
            '[.values[] | select( (.build_number < ($buildNumber | tonumber)) and (.target.ref_type == "branch") and (.target.ref_name==$branchName) ) .build_number][0]')

        debug "PreviousBuildNumber: $previousbuildNumber"
        
        # Check to see if previousBuildNumber is still not set.
        # This is most-likely indicative of the first build of this branch
        if [[ -z "$previousbuildNumber" ]] || [[ $previousbuildNumber == "null" ]]; then
            debug "PreviousBuildNumber is not set. Most likely this is the first build of this branch, retrieving single commit info for $commitHash"
            getSingleGitCommit
            if [[ $? -ne 0 ]]; then
                error "Could not retrieve commit info for $commitHash (this is probably the first build for branch: $gitBranch)"
                return 1
            fi
        else
            # we have a build number, need to get previous commit hash
            prevCommitHashURL="$pipelineBaseUrl/$previousbuildNumber"
            debug "Getting previous commit hash from: $prevCommitHashURL"
            prevFullHash=$(curl "${optionalAuthArgs[@]}" -s -X GET "$prevCommitHashURL" | jq  '.target.commit.hash')
            
            debug "prevFullHash: $prevFullHash"
            if [[ -z "$prevFullHash" ]] || [[ $prevFullHash == "null" ]]; then
                error "Could not retrieve prevFullHash to generate build-information!"
                return 1
            fi
            
            prevHash="${prevFullHash%\"}"
            prevHash="${prevFullHash#\"}"
            commitHash="${commitHash%\"}"
            commitHash="${commitHash#\"}"
            
            prevHash=$(echo $prevHash | cut -b 1-7)
            commitHash=$(echo $commitHash | cut -b 1-7)
            
            debug "prevHash: $prevHash"
            debug "commitHash: $commitHash"

            if [[ -z "$prevHash" ]] || [[ $prevHash == "null" ]]; then
                error "Calculated prevHash is not set, cannot get commit information"
                return 1
            fi

            if [[ $prevHash == $commitHash ]]; then
                debug "Previous commit hash same as current commit hash, retrieving single commit info for $commitHash"
                getSingleGitCommit
                if [[ $? -ne 0 ]]; then
                    error "Could not retrieve commit info for $commitHash (both the current and previous commits are the same)"
                    return 1
                fi
            else
                debug "Previous commit hash different to current commit hash"
                getMultipleGitCommits
                if [[ $? -ne 0 ]]; then
                    error "Could not retrieve commit info between $prevHash and $commitHash"
                    return 1
                fi
            fi
        fi
    fi

    info "Building commit json"
    if [[ -f "git-commits.log" ]]; then
        debug "git-commits.log exists"
        # Create Commits Json
        local counter=0
        while read l; do
            local hash=$(echo "$l" | cut -d' ' -f1)
            local msg=$(echo "$l" | cut -d' ' -f 2-)
            local linkUrl="$gitOrigin/commits/$hash"
            local commit=$(jq -n \
                --arg id "$hash" \
                --arg url "$linkUrl" \
                --arg msg "$msg" \
                '{Id: $id, LinkUrl: $url, Comment: $msg}' \
                )
                
            if [ $counter -eq 0 ]
                then
                commits="$commit"
                else
                commits="$commits,$commit"
            fi
            counter=$(expr $counter + 1)
        done < git-commits.log
        
        debug "Deleting temporary git-commits.log file"
        rm git-commits.log
    fi
}

createBuildInfo() {

    workspace=$BITBUCKET_REPO_OWNER
    repository=$BITBUCKET_REPO_SLUG
    buildNumber=$BITBUCKET_BUILD_NUMBER
    commitHash=$BITBUCKET_COMMIT
    gitBranch=$BITBUCKET_BRANCH
    gitOrigin=$BITBUCKET_GIT_HTTP_ORIGIN
    buildUrl="https://bitbucket.org/$workspace/$repository/addon/pipelines/home#!/results/$buildNumber"

    debug "Workspace: $workspace"
    debug "Repository: $repository"
    debug "BuildNumber: $buildNumber"
    debug "CommitHash: $commitHash"
    debug "GitBranch: $gitBranch"
    debug "GitOrigin: $gitOrigin"
    debug "BuildUrl: $buildUrl"

    commits=''

    getCommitInfo
    commitResult=$?
    debug "Commit result: ${commitResult}"
    if [[ ${commitResult} -ne 0 ]]; then
        if [[ $IGNORE_COMMIT_ERRORS == "false" ]]; then
            fail "Unable to retrieve commit information. To ignore, set IGNORE_COMMIT_ERRORS to true."
        else
            warn "Unable to retrieve commit information. IGNORE_COMMIT_ERRORS is true so these errors will be ignored."
            commits=''
        fi
    fi
    
    debug "Creating build-info json"
    local buildInfo=$(jq -n \
            --arg be "BitBucket" \
            --arg br "$gitBranch" \
            --arg bn "$buildNumber" \
            --arg bu "$buildUrl" \
            --arg vcs "Git" \
            --arg vcr "$gitOrigin" \
            --arg vcn "$commitHash" \
            --argjson cmt "[$commits]" \
            '{BuildEnvironment: $be, Branch: $br, BuildNumber: $bn, BuildUrl: $bu, VcsType: $vcs, VcsRoot: $vcr, VcsCommitNumber: $vcn, Commits: $cmt}' \
            )
    debug "Serialized Octopus build information"
    debug "${buildInfo}"
    debug "Writing out octopus.buildinfo file"
    echo $buildInfo > octopus.buildinfo
}

# Main entry point
build-information() {

    # Optional parameters
    MULTIPLE_COMMITS=${MULTIPLE_COMMITS:="False"}
    MULTIPLE_COMMITS=$(echo "$MULTIPLE_COMMITS" | tr '[:upper:]' '[:lower:]')

    IGNORE_COMMIT_ERRORS=${IGNORE_COMMIT_ERRORS:="False"}
    IGNORE_COMMIT_ERRORS=$(echo "$IGNORE_COMMIT_ERRORS" | tr '[:upper:]' '[:lower:]')

    debug "Creating PACKAGE_IDS Array"
    init_array_var 'PACKAGE_IDS'

    PACKAGE_IDS_LENGTH=${#PACKAGE_IDS[@]}
    if [[ "${PACKAGE_IDS_LENGTH}" -eq 0 ]]; then
        fail "PACKAGE_IDS variable missing any values."
    fi

    # Check for mandatory values.
    if isNotSet "${OCTOPUS_SERVER}"; then
        fail "OCTOPUS_SERVER variable missing."
    fi
    if isNotSet "${OCTOPUS_APIKEY}"; then
        fail "OCTOPUS_APIKEY variable missing."
    fi

    # Conditionally mandatory values
    if isSet "${AUTH_USERNAME}"; then
        if isNotSet "${AUTH_PASSWORD}"; then
            fail "AUTH_USERNAME variable set, but AUTH_PASSWORD variable value is missing."
        fi
    fi
    if isSet "${AUTH_PASSWORD}"; then
        if isNotSet "${AUTH_USERNAME}"; then
            fail "AUTH_PASSWORD variable set, but AUTH_USERNAME variable value is missing."
        fi
    fi
    
    args=()
    for packageid in "${PACKAGE_IDS[@]}"
    do
        args+=("--package-id=${packageid}")
    done

    args+=("--server=${OCTOPUS_SERVER}")
    args+=("--apiKey=${OCTOPUS_APIKEY}")

    optionalArgs=()

    if isSet "${OCTOPUS_SPACE}"; then
        optionalArgs+=("--space=${OCTOPUS_SPACE}")
    fi

    if isSet "${VERSION}"; then
        optionalArgs+=("--version=${VERSION}")
    fi

    if isSet "${OVERWRITE_MODE}"; then
        if [[ ! " ${overwrite_modes[@]} " =~ " ${OVERWRITE_MODE} " ]]; then
            fail "OverwriteMode '$OVERWRITE_MODE' is not supported. Please choose one of: ${overwrite_list%??}"
        else
            optionalArgs+=("--overwrite-mode=${OVERWRITE_MODE}")
        fi
    fi
    
    if isSet "${FILE}"; then
        debug "Using specified file for build info json"
        optionalArgs+=("--file=${FILE}")
    else
        debug "Creating new octopus.buildinfo json file"
        createBuildInfo
        args+=("--file=octopus.buildinfo")
    fi
    
    info "Running: octo build-information ${args[@]} ${optionalArgs[@]} ${EXTRA_ARGS[@]}"
    octo build-information "${args[@]}" "${optionalArgs[@]}" "${EXTRA_ARGS[@]}"
}