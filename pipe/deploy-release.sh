#!/usr/bin/env bash

# Main entry point
deploy-release() {

    debug "Creating DEPLOY_TO Array"
    init_array_var 'DEPLOY_TO'

    DEPLOY_TO_LENGTH=${#DEPLOY_TO[@]}
    if [[ "${DEPLOY_TO_LENGTH}" -eq 0 ]]; then
        fail "ERROR: DEPLOY_TO variable missing any values."
    fi

    # Check for mandatory values.
    if isNotSet "${OCTOPUS_SERVER}"; then
        fail "ERROR: OCTOPUS_SERVER variable missing."
    fi
    if isNotSet "${OCTOPUS_APIKEY}"; then
        fail "ERROR: OCTOPUS_APIKEY variable missing."
    fi
    if isNotSet "${PROJECT}"; then
        fail "ERROR: PROJECT variable missing."
    fi
    if isNotSet "${RELEASE_NUMBER}"; then
        fail "ERROR: RELEASE_NUMBER variable missing."
    fi  

    args=()

    args+=("--server=${OCTOPUS_SERVER}")
    args+=("--apiKey=${OCTOPUS_APIKEY}")
    args+=("--project=${PROJECT}")
    args+=("--releaseNumber=${RELEASE_NUMBER}")

    for deployto in "${DEPLOY_TO[@]}"
    do
        args+=("--deployTo=${deployto}")
    done   

    optionalArgs=()

    if isSet "${OCTOPUS_SPACE}"; then
        optionalArgs+=("--space=${OCTOPUS_SPACE}")
    fi

    if isFlagSet "${PROGRESS}"; then
        optionalArgs+=("--progress")
    fi

    info "Running: octo deploy-release ${args[@]} ${optionalArgs[@]} ${EXTRA_ARGS[@]}"
    octo deploy-release "${args[@]}" "${optionalArgs[@]}" "${EXTRA_ARGS[@]}"
}