#!/usr/bin/env bash

# Main entry point
run-runbook() {

    debug "Creating ENVIRONMENTS Array"
    init_array_var 'ENVIRONMENTS'

    ENVIRONMENTS_LENGTH=${#ENVIRONMENTS[@]}
    if [[ "${ENVIRONMENTS_LENGTH}" -eq 0 ]]; then
        fail "ERROR: ENVIRONMENTS variable missing any values."
    fi

    # Check for mandatory values.
    if isNotSet "${OCTOPUS_SERVER}"; then
        fail "ERROR: OCTOPUS_SERVER variable missing."
    fi
    if isNotSet "${OCTOPUS_APIKEY}"; then
        fail "ERROR: OCTOPUS_APIKEY variable missing."
    fi
    if isNotSet "${PROJECT}"; then
        fail "ERROR: PROJECT variable missing."
    fi
    if isNotSet "${RUNBOOK}"; then
        fail "ERROR: RUNBOOK variable missing."
    fi

    args=()

    args+=("--server=${OCTOPUS_SERVER}")
    args+=("--apiKey=${OCTOPUS_APIKEY}")
    args+=("--project=${PROJECT}")
    args+=("--runbook=${RUNBOOK}")

    for env in "${ENVIRONMENTS[@]}"
    do
        args+=("--environment=${env}")
    done   

    optionalArgs=()

    if isSet "${SNAPSHOT}"; then
        optionalArgs+=("--snapshot=${SNAPSHOT}")
    fi

    if isSet "${OCTOPUS_SPACE}"; then
        optionalArgs+=("--space=${OCTOPUS_SPACE}")
    fi

    if isFlagSet "${PROGRESS}"; then
        optionalArgs+=("--progress")
    fi

    info "Running: octo run-runbook ${args[@]} ${optionalArgs[@]} ${EXTRA_ARGS[@]}"
    octo run-runbook "${args[@]}" "${optionalArgs[@]}" "${EXTRA_ARGS[@]}"
}