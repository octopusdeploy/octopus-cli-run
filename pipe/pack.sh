#!/usr/bin/env bash

PACK_FORMATS=("zip" "nupkg")

# Main entry point
pack() {

    # Check for mandatory parameters
    if isNotSet "${ID}"; then
        fail "ERROR: ID variable missing."
    fi
    if isNotSet "${FORMAT}"; then
        fail "ERROR: FORMAT variable missing."
    fi
    if isNotSet "${VERSION}"; then
        fail "ERROR: VERSION variable missing."
    fi
    
    FORMAT=$(echo "$FORMAT" | tr '[:upper:]' '[:lower:]')

    if [[ ! " ${PACK_FORMATS[@]} " =~ " ${FORMAT} " ]]; then
        printf -v PACK_FORMATS_LIST "%s, " "${PACK_FORMATS[@]}"
        fail "ERROR: Format '$FORMAT' is not supported. Please choose one of: ${PACK_FORMATS_LIST%??}"
    fi

    args=()
    
    args+=("--id=${ID}")
    args+=("--version=${VERSION}")
    args+=("--format=${FORMAT}")
    
    optionalArgs=()

    if isSet "${SOURCE_PATH}"; then
        optionalArgs+=("--basePath=${SOURCE_PATH}")
    fi

    if isSet "${OUTPUT_PATH}"; then
        optionalArgs+=("--outFolder=${OUTPUT_PATH}")
    fi

    info "Running: octo pack ${args[@]} ${optionalArgs[@]} ${EXTRA_ARGS[@]}"
    octo pack "${args[@]}" "${optionalArgs[@]}" "${EXTRA_ARGS[@]}"
}