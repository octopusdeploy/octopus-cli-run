#!/usr/bin/env bash

# Main entry point
create-release() {

    # Check for mandatory values.
    if isNotSet "${OCTOPUS_SERVER}"; then
        fail "ERROR: OCTOPUS_SERVER variable missing."
    fi
    if isNotSet "${OCTOPUS_APIKEY}"; then
        fail "ERROR: OCTOPUS_APIKEY variable missing."
    fi
    if isNotSet "${PROJECT}"; then
        fail "ERROR: PROJECT variable missing."
    fi

    args=()
    
    args+=("--server=${OCTOPUS_SERVER}")
    args+=("--apiKey=${OCTOPUS_APIKEY}")
    args+=("--project=${PROJECT}")

    optionalArgs=()

    if isSet "${OCTOPUS_SPACE}"; then
        optionalArgs+=("--space=${OCTOPUS_SPACE}")
    fi

    if isSet "${RELEASE_NUMBER}"; then
        optionalArgs+=("--releaseNumber=${RELEASE_NUMBER}")
    fi

    if isSet "${GIT_COMMIT}"; then
        optionalArgs+=("--gitCommit=${GIT_COMMIT}")
    fi

    if isSet "${GIT_REF}"; then
        optionalArgs+=("--gitRef=${GIT_REF}")
    fi

    info "Running: octo create-release ${args[@]} ${optionalArgs[@]} ${EXTRA_ARGS[@]}"
    octo create-release "${args[@]}" "${optionalArgs[@]}" "${EXTRA_ARGS[@]}"
}