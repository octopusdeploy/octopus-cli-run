#!/usr/bin/env bats

setup() {
  DOCKERHUB_IMAGE=${DOCKERHUB_IMAGE:="octopipes/octo"}
  DOCKERHUB_TAG=${DOCKERHUB_TAG:="test"}
  IMAGE_NAME="${DOCKERHUB_IMAGE}:${DOCKERHUB_TAG}"
}

teardown() {
  echo "Running teardown"
}

@test "deploy-release: Missing DEPLOY_TO variable will fail with expected error" {
    
    EXPECTED_ERROR="ERROR: DEPLOY_TO variable missing any values."
       
    run docker run \
        -e CLI_COMMAND="deploy-release" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        $IMAGE_NAME

    echo "Output: $output"
    echo "Status: $status"

    [[ "$status" -eq 1 ]]
    [[ "${output}" == *"$EXPECTED_ERROR"* ]]
}

@test "deploy-release: Missing OCTOPUS_SERVER variable will fail with expected error" {
    
    EXPECTED_ERROR="ERROR: OCTOPUS_SERVER variable missing."
       
    run docker run \
        -e CLI_COMMAND="deploy-release" \
        -e OCTOPUS_APIKEY="XXXXXX" \
        -e DEPLOY_TO_COUNT="1" \
        -e DEPLOY_TO_0="Development" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        $IMAGE_NAME

    echo "Output: $output"
    echo "Status: $status"

    [[ "$status" -eq 1 ]]
    [[ "${output}" == *"$EXPECTED_ERROR"* ]]
}
