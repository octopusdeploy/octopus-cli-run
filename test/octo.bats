#!/usr/bin/env bats

setup() {
  DOCKERHUB_IMAGE=${DOCKERHUB_IMAGE:="octopipes/octo"}
  DOCKERHUB_TAG=${DOCKERHUB_TAG:="test"}
  IMAGE_NAME="${DOCKERHUB_IMAGE}:${DOCKERHUB_TAG}"
}

teardown() {
  echo "Running teardown"
}

@test "octo: 'invalid_command' fails with expected error message" {
    
    EXPECTED_ERROR="Command: invalid_command is not supported. Please choose one of: pack, push, build-information, create-release, deploy-release"
       
    run docker run \
        -e CLI_COMMAND="invalid_command" \
        -e DEBUG="false" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        $IMAGE_NAME

    echo "Output: $output"
    echo "Status: $status"

    [[ "$status" -eq 1 ]]
    [[ "${output}" == *"$EXPECTED_ERROR"* ]]

}