#!/usr/bin/env bats

setup() {
  DOCKERHUB_IMAGE=${DOCKERHUB_IMAGE:="octopipes/octo"}
  DOCKERHUB_TAG=${DOCKERHUB_TAG:="test"}
  IMAGE_NAME="${DOCKERHUB_IMAGE}:${DOCKERHUB_TAG}"
}

teardown() {
  echo "Running teardown"
}

@test "run-runbook: Missing ENVIRONMENTS variable will fail with expected error" {
    
    EXPECTED_ERROR="ERROR: ENVIRONMENTS variable missing any values."
       
    run docker run \
        -e CLI_COMMAND="run-runbook" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        $IMAGE_NAME

    echo "Output: $output"
    echo "Status: $status"

    [[ "$status" -eq 1 ]]
    [[ "${output}" == *"$EXPECTED_ERROR"* ]]
}

@test "run-runbook: Missing OCTOPUS_SERVER variable will fail with expected error" {
    
    EXPECTED_ERROR="ERROR: OCTOPUS_SERVER variable missing."
       
    run docker run \
        -e CLI_COMMAND="run-runbook" \
        -e OCTOPUS_APIKEY="XXXXXX" \
        -e ENVIRONMENTS_COUNT="1" \
        -e ENVIRONMENTS_0="Development" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        $IMAGE_NAME

    echo "Output: $output"
    echo "Status: $status"

    [[ "$status" -eq 1 ]]
    [[ "${output}" == *"$EXPECTED_ERROR"* ]]
}
