#!/usr/bin/env bats

setup() {
  DOCKERHUB_IMAGE=${DOCKERHUB_IMAGE:="octopipes/octo"}
  DOCKERHUB_TAG=${DOCKERHUB_TAG:="test"}
  IMAGE_NAME="${DOCKERHUB_IMAGE}:${DOCKERHUB_TAG}"
}

teardown() {
  echo "Running teardown"
}

@test "build-information: Missing PACKAGE_IDS variable will fail with expected error" {
    
    EXPECTED_ERROR="PACKAGE_IDS variable missing any values."
       
    run docker run \
        -e CLI_COMMAND="build-information" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        $IMAGE_NAME

    echo "Output: $output"
    echo "Status: $status"

    [[ "$status" -eq 1 ]]
    [[ "${output}" == *"$EXPECTED_ERROR"* ]]
}

@test "build-information: Missing OCTOPUS_SERVER variable will fail with expected error" {
    
    EXPECTED_ERROR="OCTOPUS_SERVER variable missing."
       
    run docker run \
        -e CLI_COMMAND="build-information" \
        -e OCTOPUS_APIKEY="XXXXXX" \
        -e PACKAGE_IDS_COUNT="1" \
        -e PACKAGE_IDS_0="packageA" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        $IMAGE_NAME

    echo "Output: $output"
    echo "Status: $status"

    [[ "$status" -eq 1 ]]
    [[ "${output}" == *"$EXPECTED_ERROR"* ]]
}

@test "build-information: Invalid supplied OVERWRITE_MODE will fail with expected error" {
    
    EXPECTED_ERROR="OverwriteMode 'Invalid_OVERWRITE_MODE' is not supported. Please choose one of: FailIfExists, OverwriteExisting, IgnoreIfExists"
       
    run docker run \
        -e CLI_COMMAND="build-information" \
        -e OCTOPUS_SERVER="https://example-octopus" \
        -e OCTOPUS_APIKEY="XXXXXX" \
        -e OVERWRITE_MODE="Invalid_OVERWRITE_MODE" \
        -e PACKAGE_IDS_COUNT="1" \
        -e PACKAGE_IDS_0="packageA" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        $IMAGE_NAME

    echo "Output: $output"
    echo "Status: $status"

    [[ "$status" -eq 1 ]]
    [[ "${output}" == *"$EXPECTED_ERROR"* ]]

}
