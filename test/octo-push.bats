#!/usr/bin/env bats

setup() {
  DOCKERHUB_IMAGE=${DOCKERHUB_IMAGE:="octopipes/octo"}
  DOCKERHUB_TAG=${DOCKERHUB_TAG:="test"}
  IMAGE_NAME="${DOCKERHUB_IMAGE}:${DOCKERHUB_TAG}"
}

teardown() {
  echo "Running teardown"
}

@test "push: Missing PACKAGES variable will fail with expected error" {
    
    EXPECTED_ERROR="ERROR: PACKAGES variable missing any values."
       
    run docker run \
        -e CLI_COMMAND="push" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        $IMAGE_NAME

    echo "Output: $output"
    echo "Status: $status"

    [[ "$status" -eq 1 ]]
    [[ "${output}" == *"$EXPECTED_ERROR"* ]]
}

@test "push: Missing OCTOPUS_SERVER variable will fail with expected error" {
    
    EXPECTED_ERROR="ERROR: OCTOPUS_SERVER variable missing."
       
    run docker run \
        -e CLI_COMMAND="push" \
        -e OCTOPUS_APIKEY="XXXXXX" \
        -e PACKAGES_COUNT="1" \
        -e PACKAGES_0="packageA.1.0.0.0.zip" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        $IMAGE_NAME

    echo "Output: $output"
    echo "Status: $status"

    [[ "$status" -eq 1 ]]
    [[ "${output}" == *"$EXPECTED_ERROR"* ]]
}

@test "push: Invalid supplied OVERWRITE_MODE will fail with expected error" {
    
    EXPECTED_ERROR="ERROR: OverwriteMode 'Invalid_OVERWRITE_MODE' is not supported. Please choose one of: FailIfExists, OverwriteExisting, IgnoreIfExists"
       
    run docker run \
        -e CLI_COMMAND="push" \
        -e OCTOPUS_SERVER="https://example-octopus" \
        -e OCTOPUS_APIKEY="XXXXXX" \
        -e OVERWRITE_MODE="Invalid_OVERWRITE_MODE" \
        -e PACKAGES_COUNT="1" \
        -e PACKAGES_0="packageA.1.0.0.0.zip" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        $IMAGE_NAME

    echo "Output: $output"
    echo "Status: $status"

    [[ "$status" -eq 1 ]]
    [[ "${output}" == *"$EXPECTED_ERROR"* ]]

}
