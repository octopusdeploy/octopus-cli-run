#!/usr/bin/env bats

setup() {
  DOCKERHUB_IMAGE=${DOCKERHUB_IMAGE:="octopipes/octo"}
  DOCKERHUB_TAG=${DOCKERHUB_TAG:="test"}
  IMAGE_NAME="${DOCKERHUB_IMAGE}:${DOCKERHUB_TAG}"

  # generated
  ZIP_RANDOM_NUMBER=$RANDOM
  NUPKG_RANDOM_NUMBER=$RANDOM

  # locals
  ID="MyCompany.MyApp"
  VERSION="1.0.0.0"
  SOURCE_PATH="test/pack"
  OUTPUT_PATH="test/pack/out"

  EXPECTED_ZIP="$ID.$VERSION.zip"
  EXPECTED_NUPKG="$ID.$VERSION.nupkg"

  # Create test output dir
  rm -rf test/pack/out
  mkdir test/pack/out/extract -p
  
  echo "Creating files with random content"
  echo $ZIP_RANDOM_NUMBER > test/pack/test-zip-content.txt
  echo $NUPKG_RANDOM_NUMBER > test/pack/test-nupkg-content.txt
}

teardown() {
  echo "Cleaning up files"
  chmod -R a+rwx test/pack/out/
  rm -rf test/pack/out
}

@test "pack: Missing ID variable will fail with expected error" {
    
    EXPECTED_ERROR="ERROR: ID variable missing."
       
    run docker run \
        -e CLI_COMMAND="pack" \
        -e FORMAT="zip" \
        -e VERSION="${VERSION}" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        $IMAGE_NAME

    echo "Output: $output"
    echo "Status: $status"

    [[ "$status" -eq 1 ]]
    [[ "${output}" == *"$EXPECTED_ERROR"* ]]
}

@test "pack: Invalid supplied format will fail with expected error" {
    
    EXPECTED_ERROR="ERROR: Format 'tar' is not supported. Please choose one of: zip, nupkg"
       
    run docker run \
        -e CLI_COMMAND="pack" \
        -e ID="${ID}" \
        -e FORMAT="tar" \
        -e VERSION="${VERSION}" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        $IMAGE_NAME

    echo "Output: $output"
    echo "Status: $status"

    [[ "$status" -eq 1 ]]
    [[ "${output}" == *"$EXPECTED_ERROR"* ]]

}

@test "pack: Create Zip package" {
    
    run docker run \
        -e CLI_COMMAND="pack" \
        -e ID="${ID}" \
        -e FORMAT="Zip" \
        -e VERSION="${VERSION}" \
        -e SOURCE_PATH="${SOURCE_PATH}" \
        -e OUTPUT_PATH="${OUTPUT_PATH}" \
        -e DEBUG="false" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        $IMAGE_NAME

    echo "Status: $status"
    echo "$output"
    
    [[ "$status" -eq 0 ]]

    # Verify
    unzip "test/pack/out/$EXPECTED_ZIP" -d test/pack/out/extract
    run cat "test/pack/out/extract/test-zip-content.txt"
    echo "Output: $output"
    [[ "${status}" -eq 0 ]]
    [[ "${output}" == *"$ZIP_RANDOM_NUMBER"* ]]

}

@test "pack: Create NuPkg package" {
    
    run docker run \
        -e CLI_COMMAND="pack" \
        -e ID="${ID}" \
        -e FORMAT="NuPkg" \
        -e VERSION="${VERSION}" \
        -e SOURCE_PATH="${SOURCE_PATH}" \
        -e OUTPUT_PATH="${OUTPUT_PATH}" \
        -e DEBUG="false" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        $IMAGE_NAME

    echo "Status: $status"
    echo "Output: $output"
    [[ "$status" -eq 0 ]]

    # Verify
    unzip "test/pack/out/$EXPECTED_NUPKG" -d test/pack/out/extract
    run cat "test/pack/out/extract/test-nupkg-content.txt"
    echo "Output: $output"
    [[ "${status}" -eq 0 ]]
    [[ "${output}" == *"$NUPKG_RANDOM_NUMBER"* ]]

}