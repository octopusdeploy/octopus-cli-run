# Bitbucket Pipelines Pipe: octopus-cli-run

Runs commands from the [Octopus CLI](https://octopus.com/docs/octopus-rest-api/octopus-cli) command-line tool (built on top of the REST API), allowing you to package your files or build artifacts, push packages and build information, and create & deploy releases.

**Please note:** 

This is an **experimental** pipe for working with the Octopus CLI. This pipe is provided as-is, with no support available.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: octopusdeploy/octopus-cli-run:0.41.0
  variables:
    CLI_COMMAND: "<string>"
    # EXTRA_ARGS: ['<string>','<string>' ..] # Optional
    # DEBUG: "<boolean>" # Optional
```

## Variables

As the Pipe contains a number of [supported commands](#markdown-header-supported-commands), the variables required depend on which command you are using. See below for further details of variables required for each command.

### Global variables

The following global variables are defined for use with all commands:

| Variable              | Type   | Usage                                                       |
| --------------------- | ------ | ----------------------------------------------------------- |
| CLI_COMMAND (*)       | String | The CLI command to run; e.g. `pack`, `push`. |
| EXTRA_ARGS            | String Array | Include any additional command-line arguments to the specified command. |
| DEBUG                 | Boolean| Turn on extra debug information; Default: `false`. |

_(*) = required variable._

### Pack variables

The following variables are defined for use with the `pack` command:

| Variable              | Type   | Usage                                                       |
| --------------------- | ------ | ----------------------------------------------------------- |
| ID (*)                | String | The ID of the package; e.g. `MyCompany.MyApp`. |
| FORMAT (*)            | String | Package format; Options are: `Zip`, `NuPkg`. |
| VERSION (*)           | String | The version of the package; must be a valid Semantic Version. |
| SOURCE_PATH           | String | The root folder containing files and folders to pack; Default: `'.'`. |
| OUTPUT_PATH           | String | The folder into which the generated package file will be written; Default: `'.'`. |

_(*) = required variable._

### Push variables

The following variables are defined for use with the `push` command:

| Variable              | Type   | Usage                                                       |
| --------------------- | ------ | ----------------------------------------------------------- |
| OCTOPUS_SERVER (*)    | String | The base URL for your Octopus Server, e.g. `https://octopus.example.com/`. |
| OCTOPUS_APIKEY (*)    | String | Your API key. You can get your API key from your user page in the Octopus Web Portal. |
| PACKAGES (*)          | String Array | Package files to push. Specify multiple packages by adding the path to each as an array item: `['/path/to/package1/MyCompany.MyApp.zip','/path/to/package2/MyCompany.MyApp2.nupkg']`. |
| OCTOPUS_SPACE         | String | The Name or ID of a space within which this command will be executed. Default: The default space. |
| OVERWRITE_MODE        | String | Determines behavior if the package already exists in the repository. Options are: `FailIfExists`, `OverwriteExisting`, and `IgnoreIfExists`; Default: `FailIfExists`. |

_(*) = required variable._

### Build Information variables

The following variables are defined for use with the `build-information` command:

| Variable              | Type   | Usage                                                       |
| --------------------- | ------ | ----------------------------------------------------------- |
| OCTOPUS_SERVER (*)    | String | The base URL for your Octopus Server, e.g. `https://octopus.example.com/`. |
| OCTOPUS_APIKEY (*)    | String | Your API key. You can your API key from your user page in the Octopus Web Portal. |
| PACKAGE_IDS (*)       | String Array | The ID of the package the build information relates to. Specify multiple packages by adding each as an array item: `['MyCompany.MyApp','MyCompany.MyApp2']`. |
| OCTOPUS_SPACE         | String | The Name or ID of a space within which this command will be executed. Default: The default space. |
| VERSION               | String | The version of the package. If multiple packages are specified, the version will apply to all; Default: timestamp-based version. |
| FILE                  | String | The path to an existing Octopus Build Information Json file. If not specified, a file containing build info will be created by the pipe named: `octopus.buildinfo`. |
| MULTIPLE_COMMITS      | Boolean| Try and fetch multiple commits using `git log`. See the [docs](#markdown-header-build-information-command) for more details. Default: `false`. Ignored when `FILE` is specified. |
| AUTH_USERNAME      | String | [App Password](https://developer.atlassian.com/cloud/bitbucket/rest/intro/#app-passwords) Username to use when querying for commit info from BitBucket private repositories. Not required for public repos.  *Ignored when `FILE` is specified.* |
| AUTH_PASSWORD      | String | [App Password](https://developer.atlassian.com/cloud/bitbucket/rest/intro/#app-passwords) to use when querying for commit info from BitBucket private repositories. Not required for public repos.  *Ignored when `FILE` is specified.* |
| IGNORE_COMMIT_ERRORS  | Boolean| Ignore any errors when retrieving multiple commits. See the [docs](#markdown-header-build-information-command) for more details. Default: `false`. *Ignored when `FILE` is specified.* |
| OVERWRITE_MODE        | String | Determines behavior if the package already exists in the repository. Options are: `FailIfExists`, `OverwriteExisting`, and `IgnoreIfExists`; Default: `FailIfExists`. |

_(*) = required variable._

### Create release variables

The following variables are defined for use with the `create-release` command:

| Variable              | Type   | Usage                                                       |
| --------------------- | ------ | ----------------------------------------------------------- |
| OCTOPUS_SERVER (*)    | String | The base URL for your Octopus Server, e.g. `https://octopus.example.com/`. |
| OCTOPUS_APIKEY (*)    | String | Your API key. You can get your API key from your user page in the Octopus Web Portal. |
| PROJECT (*)           | String | The Name or ID of the project to create a release for. |
| OCTOPUS_SPACE         | String | The Name or ID of a space within which this command will be executed. Default: The default space. |
| RELEASE_NUMBER        | String | The Version / Release number to used for the new release. |
| GIT_COMMIT            | String | The Git commit to use when creating the release. Use in conjunction with the `GIT_REF` parameter to select any previous commit. |
| GIT_REF               | String | The Git reference to use when creating the release. |

_(*) = required variable._

### Deploy release variables

The following variables are defined for use with the `deploy-release` command:

| Variable              | Type   | Usage                                                       |
| --------------------- | ------ | ----------------------------------------------------------- |
| OCTOPUS_SERVER (*)    | String | The base URL for your Octopus Server, e.g. `https://octopus.example.com/`. |
| OCTOPUS_APIKEY (*)    | String | Your API key. You can get your API key from your user page in the Octopus Web Portal. |
| PROJECT (*)           | String | The Name or ID of the project to deploy the release for. |
| DEPLOY_TO (*)         | String Array | Name or ID of the environment to deploy to, e.g. `Production` or `Environments-1`; Specify multiple environments by adding each as an array item: `['Development','Staging']`. |
| RELEASE_NUMBER (*)    | String | The Version / Release number of the release to deploy. Specify `latest` for the latest release. |
| OCTOPUS_SPACE         | String | The Name or ID of a space within which this command will be executed. Default: The default space. |
| PROGRESS              | Boolean| Show progress of the deployment. Default: `false`. |

_(*) = required variable._

### Run runbook variables

The following variables are defined for use with the `run-runbook` command:

| Variable              | Type   | Usage                                                       |
| --------------------- | ------ | ----------------------------------------------------------- |
| OCTOPUS_SERVER (*)    | String | The base URL for your Octopus Server, e.g. `https://octopus.example.com/` |
| OCTOPUS_APIKEY (*)    | String | Your API key. You can get your API key from your user page in the Octopus Web Portal. |
| PROJECT (*)           | String | The Name or ID of the project containing the runbook. |
| RUNBOOK (*)           | String | The Name or ID of the runbook to run. |
| ENVIRONMENTS (*)      | String Array | The Name or ID of the environment to run in, e.g. `Production` or `Environments-1`; Specify multiple environments by adding each as an array item: `['Development','Staging']`. |
| SNAPSHOT              | String | The Name or ID of the runbook snapshot to run. Default: The latest published snapshot will attempt to be used. |
| OCTOPUS_SPACE         | String | The Name or ID of a space within which this command will be executed. Default: The default space. |
| PROGRESS              | Boolean| Show progress of the runbook run. Default: `false`. |

_(*) = required variable._

## Details

### Supported commands 
The currently supported commands are:

 - [pack](#markdown-header-pack-command)
 - [push](#markdown-header-push-command)
 - [build-information](#markdown-header-build-information-command)
 - [create-release](#markdown-header-create-release-command)
 - [deploy-release](#markdown-header-deploy-release-command)
 - [run-runbook](#markdown-header-run-runbook-command)

Additional *optional* parameters can be supplied to any of the commands using the `EXTRA_ARGS` array variable. For example:

`EXTRA_ARGS: ['--logLevel', 'debug', '--timeout', '60']`

### Pack command

#### Pack task output
The pipe will create a package file based on the supplied variables in the format `$ID.$VERSION.$FORMAT`. This file will be available in the directory specified by the `$OUTPUT_PATH` variable. To use the generated package in additional steps, you can make use of Bitbucket [artifacts](https://confluence.atlassian.com/bitbucket/using-artifacts-in-steps-935389074.html).

**Note:** If you are packaging multiple files, you should specify either:

 - A unique folder per run of the pipe.
 - Try to use the root directory to save your package files.

This is due to the way Bitbucket pipelines run in containers, meaning after a folder has been created by an initial run of the pipe, subsequent runs are unable to write to it.

### Push command

#### Pushing multiple packages

To push multiple packages at once, specify each one by adding the path to the package as an item in the array variable, for example:

 `PACKAGES: ['/out/package1.1.0.0.0.zip', './package2.1.0.0.0.nupkg']`

**Notes:** 

 - Spaces in package filenames are **not** supported. (e.g. `'FileName.zip'` is valid but `'File Name.zip'` is **NOT** valid). 
 - A prefix of `--package` will be automatically added to each entry in the array before calling `push`.

### Build Information command

#### Using an existing Build Information file

If you have already created an Octopus Build Information JSON file, you can supply the path to this file using the variable `FILE`.

If the `FILE` variable is not supplied, the `build-information` command will create a new Octopus Build Information JSON file, called `octopus.buildinfo`. To use the generated file in subsequent steps, you can make use of Bitbucket [artifacts](https://confluence.atlassian.com/bitbucket/using-artifacts-in-steps-935389074.html).

#### Mutiple commit support

When the variable `MULTIPLE_COMMITS` is set to `true`, the `build-information` command will attempt to build up a collection of commit information (hash and commit messages) to pass to Octopus.

**Note:** There are some limitations when using this option:

 - The command assumes that your repository is using a `VcsType` of `git` (internally it calls `git log`).
 - Commit information may not be available if you have [disabled clone on the pipeline step](https://bitbucket.org/blog/disabling-clones-in-pipelines-steps).
 - The command also makes use of the Bitbucket [REST API](https://developer.atlassian.com/bitbucket/api/2/reference/) and by default, does so **without authentication support**. To provide credentials (for private repositories), set **both** the `AUTH_USERNAME` and `AUTH_PASSWORD` variables.
 - Support for multiple commits has only been tested with direct branch-based changes. Commits that span multiple branches, for example via a Pull Request has only been tested for simple scenarios. Therefore it's possible that not every case has been covered.

If the `FILE` variable is set, then setting `MULTIPLE_COMMITS` will be ignored.

When attempting to build multiple commits from BitBucket repositories, errors may be returned. To ignore these errors and send build information to Octopus **without commit information**, set the `IGNORE_COMMIT_ERRORS` to `true`.

### Create release command

This command allows you to create a release. Specify the project to create the release for using the `PROJECT` variable. Choose the number to use for this release by specifying it in the `RELEASE_NUMBER` variable. You can also choose the `GIT_REF` for projects that are [version-controlled](https://octopus.com/docs/projects/version-control).

You can also optionally deploy the release to one or more environments as part of this command. To achieve this you must make use of the **global** `EXTRA_ARGS` array variable, and provide the appropriate options. For example: 

`EXTRA_ARGS: ['--deployTo', 'Development', '--guidedFailure', 'True']`

### Deploy release command

This command allows you to deploy releases that have already been created. Specify the project to deploy the release for using the `PROJECT` variable. Choose the number to use for this release by specifying it in the `RELEASE_NUMBER` variable.

Choose the environment(s) to deploy to by specifying them in the `DEPLOY_TO` variable using either the Name or ID, like so:

`DEPLOY_TO: ['Environments-1', 'Development', 'Staging', 'Test']`

In addition, you can supply additional arguments to this command. To achieve this you must make use of the **global** `EXTRA_ARGS` array variable and provide the appropriate options. For example: 

`EXTRA_ARGS: ['--waitForDeployment', 'true', '--tenantTag', 'VIP']`

### Run runbook command

This command allows you to run runbooks that have already been created. Specify the project where the runbook lives using the `PROJECT` variable. Choose the runbook to run by specifying it in the `RUNBOOK` variable.

Choose the environment(s) to deploy to by specifying them in the `ENVIRONMENTS` variable using either the Name or ID, like so:

`ENVIRONMENTS: ['Environments-1', 'Development', 'Staging', 'Test']`

Optionally, choose the specific snapshot of a runbook to run using the `SNAPSHOT` variable.

In addition, you can supply additional arguments to this command. To achieve this you must make use of the **global** `EXTRA_ARGS` array variable and provide the appropriate options. For example: 

`EXTRA_ARGS: ['--waitForRun', 'true', '--tenantTag', 'VIP']`

### Documentation

The commands also support more *optional* parameters than listed here. Please refer to the following documentation for more information:

* The [Octopus CLI](https://octopus.com/docs/octopus-rest-api/octopus-cli) reference
* Packaging files using [pack](https://octopus.com/docs/octopus-rest-api/octopus-cli/pack)
* Pushing packages to Octopus using [push](https://octopus.com/docs/octopus-rest-api/octopus-cli/push)
* Pushing Build Information to Octopus using [build-information](https://octopus.com/docs/octopus-rest-api/octopus-cli/build-information)
* Creating releases in Octopus using [create-release](https://octopus.com/docs/octopus-rest-api/octopus-cli/create-release)
* Deploying releases in Octopus using [deploy-release](https://octopus.com/docs/octopus-rest-api/octopus-cli/deploy-release)
* Run runbooks in Octopus using [run-runbook](https://octopus.com/docs/octopus-rest-api/octopus-cli/run-runbook)

## Examples

### Pack examples

Basic `pack` example:

```yaml
script:
  - pipe: octopusdeploy/octopus-cli-run:0.41.0
    variables:
      CLI_COMMAND: 'pack'
      ID: $PACKAGE_ID
      FORMAT: $PACKAGE_FORMAT
      VERSION: $PACKAGE_VERSION
artifacts:
  - output/*.zip
```

Advanced `pack` example:

```yaml
script:
  - pipe: octopusdeploy/octopus-cli-run:0.41.0
    variables:
      CLI_COMMAND: 'pack'
      ID: $PACKAGE_ID
      FORMAT: $PACKAGE_FORMAT
      VERSION: $PACKAGE_VERSION
      SOURCE_PATH: './src'
      OUTPUT_PATH: './output'
      DEBUG: "true"
      EXTRA_ARGS: ['--compressionlevel', 'fast']
artifacts:
  - output/*.zip
```

### Push examples

Basic `push` example:

```yaml
script:
  - pipe: octopusdeploy/octopus-cli-run:0.41.0
    variables:
      CLI_COMMAND: 'push'
      OCTOPUS_SERVER: $OCTOPUS_SERVER
      OCTOPUS_APIKEY: $OCTOPUS_APIKEY
      PACKAGES: ['/out/package.1.0.0.0.zip']
```

Advanced `push` example:

```yaml
script:
  - pipe: octopusdeploy/octopus-cli-run:0.41.0
    variables:
      CLI_COMMAND: 'push'
      OCTOPUS_SERVER: $OCTOPUS_SERVER
      OCTOPUS_APIKEY: $OCTOPUS_APIKEY
      PACKAGES: [$PACKAGE_NAME, '/out/package1.1.0.0.0.nupkg','./my package.1.0.0.0.zip']
      OCTOPUS_SPACE: $OCTOPUS_SPACE
      OVERWRITE_MODE: 'OverwriteExisting'
      EXTRA_ARGS: ['--use-delta-compression', 'false', '--timeout', '900']
```

### Build Information examples

Basic `build-information` example:

```yaml
script:
  - pipe: octopusdeploy/octopus-cli-run:0.41.0
    variables:
      CLI_COMMAND: 'build-information'
      OCTOPUS_SERVER: $OCTOPUS_SERVER
      OCTOPUS_APIKEY: $OCTOPUS_APIKEY
      PACKAGE_IDS: ['MyCompany.App']
      VERSION: $VERSION
```

Advanced `build-information` example:

```yaml
script:
  - pipe: octopusdeploy/octopus-cli-run:0.41.0
    variables:
      CLI_COMMAND: 'build-information'
      OCTOPUS_SERVER: $OCTOPUS_SERVER
      OCTOPUS_APIKEY: $OCTOPUS_APIKEY
      PACKAGE_IDS: [$PACKAGEID, 'MyCompany.App']
      OCTOPUS_SPACE: $OCTOPUS_SPACE
      VERSION: $VERSION
      MULTIPLE_COMMITS: 'true'
      AUTH_USERNAME: $AUTH_USERNAME
      AUTH_PASSWORD: $AUTH_PASSWORD
      IGNORE_COMMIT_ERRORS: 'false'
      OVERWRITE_MODE: 'OverwriteExisting'
      EXTRA_ARGS: ['--logLevel', 'information', '--timeout', '900']
```

### Create release examples

Basic `create-release` example:

```yaml
script:
  - pipe: octopusdeploy/octopus-cli-run:0.41.0
    variables:
      CLI_COMMAND: 'create-release'
      OCTOPUS_SERVER: $OCTOPUS_SERVER
      OCTOPUS_APIKEY: $OCTOPUS_APIKEY
      PROJECT: $PROJECT_NAME
      RELEASE_NUMBER: $RELEASE_NUMBER
```

Advanced `create-release` example:

```yaml
script:
  - pipe: octopusdeploy/octopus-cli-run:0.41.0
    variables:
      CLI_COMMAND: 'create-release'
      OCTOPUS_SERVER: $OCTOPUS_SERVER
      OCTOPUS_APIKEY: $OCTOPUS_APIKEY
      PROJECT: $PROJECT_NAME
      OCTOPUS_SPACE: $OCTOPUS_SPACE
      RELEASE_NUMBER: $RELEASE_NUMBER
      EXTRA_ARGS: ['--channel', 'HotFix', '--ignoreExisting', 'true']
```

### Deploy release examples

Basic `deploy-release` example:

```yaml
script:
  - pipe: octopusdeploy/octopus-cli-run:0.41.0
    variables:
      CLI_COMMAND: 'deploy-release'
      OCTOPUS_SERVER: $OCTOPUS_SERVER
      OCTOPUS_APIKEY: $OCTOPUS_APIKEY
      PROJECT: $PROJECT_NAME
      RELEASE_NUMBER: $RELEASE_NUMBER
      DEPLOY_TO: ['Development']
```

Advanced `deploy-release` example:

```yaml
script:
  - pipe: octopusdeploy/octopus-cli-run:0.41.0
    variables:
      CLI_COMMAND: 'deploy-release'
      OCTOPUS_SERVER: $OCTOPUS_SERVER
      OCTOPUS_APIKEY: $OCTOPUS_APIKEY
      PROJECT: $PROJECT_NAME
      RELEASE_NUMBER: 'latest'
      DEPLOY_TO: ['Development', 'Testing']
      OCTOPUS_SPACE: $OCTOPUS_SPACE
      PROGRESS: 'true'
      EXTRA_ARGS: ['--waitForDeployment', 'true', '--tenantTag', 'VIP']
```

### Run runbook examples

Basic `run-runbook` example:

```yaml
script:
  - pipe: octopusdeploy/octopus-cli-run:0.41.0
    variables:
      CLI_COMMAND: 'run-runbook'
      OCTOPUS_SERVER: $OCTOPUS_SERVER
      OCTOPUS_APIKEY: $OCTOPUS_APIKEY
      PROJECT: $PROJECT_NAME
      RUNBOOK: $RUNBOOK_NAME
      ENVIRONMENTS: ['Development']
```

Advanced `run-runbook` example:

```yaml
script:
  - pipe: octopusdeploy/octopus-cli-run:0.41.0
    variables:
      CLI_COMMAND: 'run-runbook'
      OCTOPUS_SERVER: $OCTOPUS_SERVER
      OCTOPUS_APIKEY: $OCTOPUS_APIKEY
      PROJECT: $PROJECT_NAME
      RUNBOOK: 'Check Database Infrastructure'
      ENVIRONMENTS: ['Development', 'Testing']
      SNAPSHOT: 'Snapshot V8GD8W7'
      OCTOPUS_SPACE: $OCTOPUS_SPACE
      PROGRESS: 'true'
      EXTRA_ARGS: ['--waitForRun', 'true', '--tenant', 'OctoPetShop']
```

## Support

This is an experimental pipe for working with the Octopus CLI. This pipe is provided as-is, with no support available.

 - Email [bitbucket@octopus.com](mailto:bitbucket@octopus.com) if you have any feedback about the Pipe.

 - Check the [Atlassian community](https://community.atlassian.com/t5/forums/postpage/choose-node/true/interaction-style/qanda?add-tags=bitbucket-pipelines,pipes) to see if your question has already been answered.