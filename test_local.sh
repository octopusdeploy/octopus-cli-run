#!/usr/bin/env bash
#!/usr/bin/env bats

runLocalTests() {
    export DOCKERHUB_IMAGE="testpipe/octopus-cli-run"
    export DOCKERHUB_TAG="test"
    echo "Building test image ${DOCKERHUB_IMAGE}:${DOCKERHUB_TAG}"
    docker build -t ${DOCKERHUB_IMAGE}:${DOCKERHUB_TAG} .
    echo "Testing image ${DOCKERHUB_IMAGE}:${DOCKERHUB_TAG}"
    bats test/*.bats
}

runLocalTests