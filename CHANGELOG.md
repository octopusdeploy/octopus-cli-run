# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.41.0

- minor: Add exception for clone directory for git operations calculating build info

## 0.40.0

- minor: Tag docker latest version and image

## 0.39.0

- minor: Add GIT_COMMIT and GIT_REF support to create-release command
- minor: Update octo version to 9.1.7

## 0.38.0

- minor: Correct hard-coded 404 in error message

## 0.37.0

- minor: Change initial cURL call to look for non 200 OK status code, rather than 404 explicitly

## 0.36.0

- minor: Correct missing auth variable error message

## 0.35.0

- minor: Rework build-information creation to exit earlier, and provide better control for getting commits with different scenarios

## 0.34.0

- minor: Spacing for hash call

## 0.33.0

- minor: Tweak optional auth args to work with curl better

## 0.32.0

- minor: Correct spelling on auth username/password validation

## 0.31.0

- minor: Remove local definition from variables and move them

## 0.30.0

- minor: Change fail calls to error calls, and return non-zero exit code

## 0.29.0

- minor: Add warning function

## 0.28.0

- minor: Add authentication support for build info, and add optional ignore flag for any commit errors
- minor: Update Dockerfile to use octo 7.4.3576

## 0.27.0

- minor: Update Dockerfile to use octo 7.4.3548

## 0.26.0

- minor: Update Dockerfile to use octo 7.4.3467

## 0.25.0

- minor: Update Dockerfile to use octo 7.4.3357

## 0.24.0

- minor: Update Dockerfile to use octo 7.4.3287

## 0.23.0

- minor: Update Dockerfile to use octo 7.4.3173

## 0.22.0

- minor: Update Dockerfile to use octo 7.4.6

## 0.21.0

- minor: Update Dockerfile to use octo 7.4.4

## 0.20.0

- minor: Update Dockerfile to use octo 7.4.2

## 0.19.0

- minor: Update build-information command to get the correct previous build

## 0.18.0

- minor: Update build-information command to correct gitBranch casing

## 0.17.0

- minor: Update build-information command to retrieve previous build number for correct branch

## 0.16.0

- minor: Update Dockerfile to use octo 7.4.1

## 0.15.0

- minor: Add run-runbook command support

## 0.14.0

- minor: Update octopusdeploy/octo base image to 7.4.0 in DOCKERFILE

## 0.13.0

- minor: Update octopusdeploy/octo base image to 7.3.7 in DOCKERFILE

## 0.12.0

- minor: Rename pipe from octo to octopus-cli-run

## 0.11.0

- minor: Streamline testing to build image once

## 0.10.0

- minor: Change name property to just octo

## 0.9.0

- minor: Add deploy-release command

## 0.8.0

- minor: Add create-release command

## 0.7.0

- minor: Add git, curl and jq to docker image

## 0.6.0

- minor: Add build-information.sh to main pipe

## 0.5.0

- minor: Add initial work for build-information

## 0.4.0

- minor: Correct check for pack variables

## 0.3.0

- minor: Add echo of commands running

## 0.2.0

- minor: Reomve execute

## 0.1.0

- minor: Add initial draft for pack+push

